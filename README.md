# MemberMailingSystem

This is a simple system to notify people via Email and SMS of something, for free.

## Setup (to be refined)
In order to use it, you need a Google Sheet shared with the (service?) user (just follow [this](https://gspread.readthedocs.io/en/latest/oauth2.html). -> Save the credentials file in the directory of this program as `CREDS.json`.

You must also follow the steps to authenticate G-Mail (https://developers.google.com/gmail/api/quickstart/js) -> Save the credentials as `email_creds.json`.

Then set up a Google Sheet in the following format:

| A         | B         | C         | D         | E          | F         | G     | H                                       | I                                                |
| ----      | ---       | --        | -         | -          | -         | -     | -                                       | -                                                |
| Any Value | Any Value | Any Value | Any Value | First Name | Last Name | Email | Phone number (in format `yyy-yyy-yyyy`) | Phone, Email, or Both (which medium to alert on) |



Copy `config.json.template` to `config.json` and update the values accordingly.
Create and update `message.txt` to have whatever you want.

### Install Dependencies

`pip3 install --user hangups google-api-python-client google-auth-httplib2 google-auth-oauthlib gspread oauth2client`

### Set Up Hangups

`hangups --manual` - Follow the instructions, and when it takes you into the client press `Control+E`
Most likely, your refresh token is `$HOME/.cache/hangups/refresh_token.txt` ($HOME is just your home directory) and update this into `config.json`. If you want to use a custom path for the hangups refresh token, you can also specify it in `config.json`.

## Run

To send out the alert, simply run `python3 sms_from_gsheet.py` (this will send emails as well).

**Note:** A lot of code for this program was pulled from the sources linked above (examples).

