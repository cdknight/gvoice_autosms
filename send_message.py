import hangups
import asyncio
import pprint
import json
import time

with open("config.json") as config_f:
        config = json.load(config_f)
cookies = hangups.auth.get_auth_stdin(config.get("refresh_token"))
client = hangups.Client(cookies)

async def _send_message(client, phone, message):

        # We need to first check if there is an existing room with the phone number. Otherwise, we create the room

	user_list, conv_list = await hangups.build_user_conversation_list(client)
	for conv in conv_list.get_all():
		print(conv.name)
		if conv.name == phone: #Check if our conversation with the phone number was already created
			await conv.send_message([hangups.ChatMessageSegment(message)])
			return

	lookup_spec = hangups.hangouts_pb2.EntityLookupSpec(phone=phone, create_offnetwork_gaia=True)

	request = hangups.hangouts_pb2.GetEntityByIdRequest(
		request_header=client.get_request_header(),
		batch_lookup_spec=[lookup_spec],
	)
	
	real_phone_res = await client.get_entity_by_id(request)
#	print(real_phone_res)
#	exit()



	gaia_ids = real_phone_res.entity_result[0].entity # Get the GOOGLE_VOICE gaia_id

	real_gaia_id = ""
	for gaia_id in gaia_ids:
		if gaia_id.entity_type == hangups.hangouts_pb2.PARTICIPANT_TYPE_GOOGLE_VOICE:
			real_gaia_id = gaia_id.id.gaia_id

	
	print("GAIA ID LIST: " + str(gaia_ids))
	print("GAIA ID: " + str(real_gaia_id))


	new_conv = hangups.hangouts_pb2.CreateConversationRequest(
		request_header=client.get_request_header(),
		type=hangups.hangouts_pb2.CONVERSATION_TYPE_GROUP,
		client_generated_id=client.get_client_generated_id(),
		invitee_id=[hangups.hangouts_pb2.InviteeID(gaia_id=real_gaia_id)],
		name=phone # Set this for future reference
	)

	resp = await client.create_conversation(new_conv)
	conv_id = resp.conversation.conversation_id.id

#	print(resp)

	print("Conversation ID is " + conv_id)

	message = hangups.hangouts_pb2.SendChatMessageRequest(
		request_header=client.get_request_header(),
		event_request_header=hangups.hangouts_pb2.EventRequestHeader(
			conversation_id=hangups.hangouts_pb2.ConversationId(
				id=conv_id
			),
			client_generated_id=client.get_client_generated_id(),
			delivery_medium=hangups.hangouts_pb2.DeliveryMedium(
				medium_type=hangups.hangouts_pb2.DELIVERY_MEDIUM_GOOGLE_VOICE,
				phone_number=hangups.hangouts_pb2.PhoneNumber(
                                       e164=config.get("phone_send")
                                )
			)
		),
		message_content=hangups.hangouts_pb2.MessageContent(
			segment=[
				hangups.ChatMessageSegment(message).serialize()
			],
		),

	)
	await client.send_chat_message(message)

async def _mass_send_messages(client, data):
	for msg_entry in data:
		await _send_message(client, msg_entry['phone'], msg_entry['message'])
		time.sleep(2)


async def _async_main(example_coroutine, client, args):
    """Run the example coroutine."""
    # Spawn a task for hangups to run in parallel with the example coroutine.
    task = asyncio.ensure_future(client.connect())

    # Wait for hangups to either finish connecting or raise an exception.
    on_connect = asyncio.Future()
    client.on_connect.add_observer(lambda: on_connect.set_result(None))
    done, _ = await asyncio.wait(
        (on_connect, task), return_when=asyncio.FIRST_COMPLETED
    )
    await asyncio.gather(*done)

    # Run the example coroutine. Afterwards, disconnect hangups gracefully and
    # yield the hangups task to handle any exceptions.
    try:
        await example_coroutine(client, **args)
    except asyncio.CancelledError:
        print("Fail")
        pass

def send_message(data):
	loop = asyncio.get_event_loop()
	
	task = asyncio.ensure_future(
		_async_main(_mass_send_messages, client, {'data': data}), loop=loop
	)	
	try:
		loop.run_until_complete(task)
	except KeyboardInterrupt:
		task.cancel()
		loop.run_until_complete(task)


