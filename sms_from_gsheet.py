import gspread
from oauth2client.service_account import ServiceAccountCredentials
import send_message
import email_mass_send
import json

with open("config.json") as config_f:
    config = json.load(config_f)

scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

credentials = ServiceAccountCredentials.from_json_keyfile_name('CREDS.json', scope)


gc = gspread.authorize(credentials)

wks = gc.open(config['sheet_name']).sheet1
first_names = wks.col_values(5)[2:]
last_names = wks.col_values(6)[2:]
emails = wks.col_values(7)[2:]
phone_numbers = wks.col_values(8)[2:]
contact_methods = wks.col_values(9)[2:]

phone_msg_list = []
email_list = []

with open("message.txt") as message_f:
    message = message_f.read()

for first_name, last_name, email, phone_number, contact_method in zip(first_names, last_names, emails, phone_numbers, contact_methods):
    print(f"{phone_number}")
    msg_to_send = f"Greetings {first_name} -\n\n" + message
    if contact_method.strip() == "Both":
        phone_msg_list.append({'phone': f"+1{phone_number}", 'message': msg_to_send})
        email_list.append({'email': email, 'message': msg_to_send})
    if contact_method.strip() == "Email":
        email_list.append({'email': email, 'message': msg_to_send})
    if contact_method.strip() == "Phone":
        phone_msg_list.append({'phone': f"+1{phone_number}", 'message': msg_to_send})

for email in email_list:
    msg = email_mass_send.create_message(config['sender'], email['email'], config['subject'], email['message'])
    email_mass_send.send_message(email_mass_send.service, config['email_account'], msg)
    
print(email_list)

print()

print(phone_msg_list)

send_message.send_message(phone_msg_list)
